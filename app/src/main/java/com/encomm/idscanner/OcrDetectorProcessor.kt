
package com.encomm.idscanner

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.text.TextBlock

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 */
class OcrDetectorProcessor internal constructor(private val context: Context) :
    Detector.Processor<TextBlock?> {

    /**
     * Called by the detector to deliver detection results.
     * If your application called for it, this could be a place to check for
     * equivalent detections by tracking TextBlocks that are similar in location and content from
     * previous frames, or reduce noise by eliminating TextBlocks that have not persisted through
     * multiple detections.
     */
    override fun receiveDetections(detections: Detections<TextBlock?>) {
        val items = detections.detectedItems
        for (i in 0 until items.size()) {
            val item = items.valueAt(i)
            if (item != null && item.value != null) {
                if (item.value.startsWith("IDKYA")) {
//                    Log.d("OcrDetectorProcessor", "Text detected! " + item.getValue().trim().replaceAll("\\s+", "").length());
//                    Log.d("OcrDetectorProcessor", "Text detected! " + item.getValue());
//                    final MrzRecord record = MrzParser.parse(item.getValue().trim().replaceAll("\\s+", ""));
                    val result =
                        item.value.trim { it <= ' ' }.replace("\\s+".toRegex(), "")
                    val name =
                        result.substring(60, 90).replace("<".toRegex(), " ").trim { it <= ' ' }
                    val id = result.substring(48, 56)
                    Log.d(
                        "OcrDetectorProcessor",
                        "Text detected! $name - $id"
                    )
                    context.startActivity(
                        Intent(context, MainActivity::class.java).putExtra(
                            "name",
                            name.replace("[0-9]".toRegex(), "")
                        ).putExtra("id", id)
                    )
                } else {
                    showMessage("Getting things right, Please wait...")
                }
            } else {
                showMessage("No Text Detected")
            }
        }
    }

    private fun showMessage(s: String) {
        val mainhandler = Handler(context.mainLooper)
        val r = Runnable { Toast.makeText(context, s, Toast.LENGTH_LONG).show() }
        mainhandler.post(r)
    }

    /**
     * Frees the resources associated with this detection processor.
     */
    override fun release() {}

}